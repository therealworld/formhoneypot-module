<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormHoneyPotModule\Core;

use Exception;
use OxidEsales\Eshop\Core\Exception\InputException;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsMonologLogger;

/**
 * central Configuration for FormHoneyPot.
 */
class FormHoneyPotHelper
{
    /** the formelement-prefix */
    protected static array $_aFormElementPrefix = [
        'user'       => 'invadr[oxuser__%s]',
        'pricealarm' => 'pa[%s]',
        'contact'    => 'editval[oxuser__%s]',
        'invite'     => 'editval[send_%s]',
    ];

    /** the FrontendController with Forms for HoneyPots */
    protected static array $_aFrontendController = [
        'AccountUserController'    => 'user',
        'ArticleDetails'           => 'pricealarm',
        'ArticleDetailsController' => 'pricealarm',
        'ContactController'        => 'contact',
        'InviteController'         => 'invite',
        'PriceAlarmController'     => 'pricealarm',
        'RegisterController'       => 'user',
        'SuggestController'        => 'invite',
        'UserComponent'            => 'user',
        'UserController'           => 'user',
    ];

    /** the FrontendController with HoneyPot Checks */
    protected static array $_aControllerWithHoneyPot = [
        'ArticleDetailsController',
        'ContactController',
        'InviteController',
        'PriceAlarmController',
        'SuggestController',
        'UserComponent',
    ];

    /** get the FrontendController with HoneyPots */
    public static function getFrontendController(): array
    {
        return array_keys(self::$_aFrontendController);
    }

    /** get the FrontendController with HoneyPot Checks */
    public static function getFrontendControllerWithHoneyPot(): array
    {
        return self::$_aControllerWithHoneyPot;
    }

    /** get the HoneyPotFormElements.
     *
     * @param string $sFrontendController - FrontendController
     */
    public static function getHoneyPotFormElements(string $sFrontendController = ''): array
    {
        $aResult = [];
        $oConfig = Registry::getConfig();
        $aTRWFormHoneyPotFormVars = $oConfig->getConfigParam('aTRWFormHoneyPotFormVars');

        $sFormElementPrefix = self::$_aFrontendController[$sFrontendController];

        foreach ($aTRWFormHoneyPotFormVars as $sVarName) {
            $sVarNameFull = sprintf(self::$_aFormElementPrefix[$sFormElementPrefix], $sVarName);
            $aResult[] = [
                'name'  => $sVarNameFull,
                'value' => self::_getRequestParameter($sVarNameFull),
            ];
        }

        return $aResult;
    }

    /** is something inside the honeypot.
     *
     * @param string $sFrontendController - FrontendController
     *
     * @throws Exception
     */
    public static function isFallenIntoHoneyPot(string $sFrontendController): bool
    {
        $bResult = false;
        $oConfig = Registry::getConfig();
        $oRequest = Registry::getRequest();

        if ($oConfig->getConfigParam('bTRWFormHoneyPotUseHoneyPot')) {
            $sFormElementPrefix = self::$_aFrontendController[$sFrontendController];
            $bThrowExceptionOnError = $oConfig->getConfigParam('bTRWFormHoneyPotThrowExceptionOnError');
            $bThrowMessageOnError = $oConfig->getConfigParam('bTRWFormHoneyPotThrowMessageOnError');

            // check if request is "fallen" into HoneyPot
            $bFallenIntoHoneyPot = false;
            $aTRWFormHoneyPotFormVars = $oConfig->getConfigParam('aTRWFormHoneyPotFormVars');
            foreach ($aTRWFormHoneyPotFormVars as $sVarName) {
                $sVarNameFull = sprintf(self::$_aFormElementPrefix[$sFormElementPrefix], $sVarName);
                if (self::_getRequestParameter($sVarNameFull)) {
                    $bFallenIntoHoneyPot = true;

                    break;
                }
            }

            // collect IP & Form
            $aRequestData = [
                'controller' => $sFrontendController,
                'clientIP'   => (
                    $oConfig->getConfigParam('bTRWFormHoneyPotCollectIP') ?
                    $_SERVER['REMOTE_ADDR'] :
                    ''
                ),
            ];

            // collect full request
            $aRequestParam = explode('[', self::$_aFormElementPrefix[$sFormElementPrefix]);
            $aRequestData = array_merge(
                $aRequestData,
                $oRequest->getRequestParameter($aRequestParam[0])
            );

            // Error Handling
            if (($bThrowExceptionOnError || $bThrowMessageOnError) && $bFallenIntoHoneyPot) {
                $bResult = true;

                if ($bThrowMessageOnError) {
                    Registry::getUtilsView()->addErrorToDisplay('ERROR_MESSAGE_HONEYPOT_MESSAGE');
                }
                if ($bThrowExceptionOnError) {
                    self::_writeToLog(
                        'ERROR_MESSAGE_HONEYPOT_EXCEPTION',
                        $aRequestData
                    );
                }
            }

            // Info Handling
            if (
                $oConfig->getConfigParam('bTRWFormHoneyPotThrowExceptionOnUse')
                && in_array(
                    $sFrontendController,
                    $oConfig->getConfigParam(
                        'aTRWFormHoneyPotUseMessageForFrontendController'
                    ),
                    true
                )
            ) {
                self::_writeToLog(
                    'INFO_MESSAGE_HONEYPOT_EXCEPTION',
                    $aRequestData,
                    'info'
                );
            }
        }

        return $bResult;
    }

    /**
     * split a Varname as array if nesseccary an return the requestvalue.
     */
    private static function _getRequestParameter(string $sVarName): string
    {
        $oRequest = Registry::getRequest();

        if (strpos($sVarName, '[')) {
            $aVarName = explode('[', $sVarName);
            $aRequestParams = $oRequest->getRequestParameter($aVarName[0]);
            $sVarValueName = trim($aVarName[1], ']');
            $sResult = $aRequestParams[$sVarValueName];
        } else {
            $sResult = $oRequest->getRequestParameter($sVarName);
        }

        return $sResult;
    }

    /**
     * write a Exeption Message to the log.
     *
     * @param string $sMessage  (oLang Variable)
     * @param string $sLogLevel (e.g. error, info, warning ... see Monolog\Logger
     *
     * @throws Exception
     */
    private static function _writeToLog(string $sMessage, array $aData = [], string $sLogLevel = 'error'): void
    {
        // help: build array to string without print_r
        $sData = str_replace('=', ':', http_build_query($aData, null, ','));

        $oEx = oxNew(InputException::class);
        $oEx->setMessage(sprintf(
            Registry::getLang()->translateString($sMessage),
            $sData
        ));

        $oLogger = ToolsMonologLogger::getLogger('trwformhoneypot');

        $oLogger->{$sLogLevel}($oEx->getMessage(), [$oEx]);
    }
}
