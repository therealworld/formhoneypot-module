<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);
// Metadata version.

use OxidEsales\Eshop\Application\Component\UserComponent as OxUserComponent;
use OxidEsales\Eshop\Application\Component\Widget\ArticleDetails as OxArticleDetails;
use OxidEsales\Eshop\Application\Controller\AccountUserController as OxAccountUserController;
use OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration as OxModuleConfiguration;
use OxidEsales\Eshop\Application\Controller\ArticleDetailsController as OxArticleDetailsController;
use OxidEsales\Eshop\Application\Controller\ContactController as OxContactController;
use OxidEsales\Eshop\Application\Controller\InviteController as OxInviteController;
use OxidEsales\Eshop\Application\Controller\PriceAlarmController as OxPriceAlarmController;
use OxidEsales\Eshop\Application\Controller\RegisterController as OxRegisterController;
use OxidEsales\Eshop\Application\Controller\SuggestController as OxSuggestController;
use OxidEsales\Eshop\Application\Controller\UserController as OxUserController;
use TheRealWorld\FormHoneyPotModule\Application\Component\UserComponent;
use TheRealWorld\FormHoneyPotModule\Application\Component\Widget\ArticleDetails;
use TheRealWorld\FormHoneyPotModule\Application\Controller\AccountUserController;
use TheRealWorld\FormHoneyPotModule\Application\Controller\Admin\ModuleConfiguration;
use TheRealWorld\FormHoneyPotModule\Application\Controller\ArticleDetailsController;
use TheRealWorld\FormHoneyPotModule\Application\Controller\ContactController;
use TheRealWorld\FormHoneyPotModule\Application\Controller\InviteController;
use TheRealWorld\FormHoneyPotModule\Application\Controller\PriceAlarmController;
use TheRealWorld\FormHoneyPotModule\Application\Controller\RegisterController;
use TheRealWorld\FormHoneyPotModule\Application\Controller\SuggestController;
use TheRealWorld\FormHoneyPotModule\Application\Controller\UserController;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwformhoneypot',
    'title' => [
        'de' => 'the-real-world - Formular Spam Honigtopf',
        'en' => 'the-real-world - Form Spam Honeypot',
    ],
    'description' => [
        'de' => 'Ein Honigtopf-Plugin für öffentliche Formulare z.b. Kontaktformular.',
        'en' => 'A Honeypot-Plugin for public forms like contact-form.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwformhoneypot'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'extend'    => [
        // Component
        OxUserComponent::class => UserComponent::class,
        // Widget
        OxArticleDetails::class => ArticleDetails::class,
        // Frontend-Controller
        OxAccountUserController::class    => AccountUserController::class,
        OxArticleDetailsController::class => ArticleDetailsController::class,
        OxContactController::class        => ContactController::class,
        OxInviteController::class         => InviteController::class,
        OxPriceAlarmController::class     => PriceAlarmController::class,
        OxRegisterController::class       => RegisterController::class,
        OxUserController::class           => UserController::class,
        OxSuggestController::class        => SuggestController::class,
        // Backend-Controller
        OxModuleConfiguration::class => ModuleConfiguration::class,
    ],
    'blocks' => [
        [
            'template' => 'module_config.tpl',
            'block'    => 'admin_module_config_var_type_arr',
            'file'     => 'Application/views/blocks/admin_module_config_var_type_arr.tpl',
        ],
        [
            'template' => 'form/contact.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_honeypot.tpl',
        ],
        [
            'template' => 'form/privatesales/invite.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_honeypot.tpl',
        ],
        [
            'template' => 'form/pricealarm.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_honeypot.tpl',
        ],
        [
            'template' => 'form/suggest.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_honeypot.tpl',
        ],
        [
            'template' => 'form/fieldset/user_billing.tpl',
            'block'    => 'form_user_billing_country',
            'file'     => '/Application/views/blocks/form_honeypot.tpl',
        ],
    ],
    'settings' => [
        [
            'group' => 'trwformhoneypotcontrol',
            'name'  => 'bTRWFormHoneyPotUseHoneyPot',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformhoneypotconfig',
            'name'  => 'aTRWFormHoneyPotFormVars',
            'type'  => 'arr',
            'value' => ['mailrepeat', 'familyname'],
        ],
        [
            'group' => 'trwformhoneypotcontrol',
            'name'  => 'bTRWFormHoneyPotThrowMessageOnError',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwformhoneypotcontrol',
            'name'  => 'bTRWFormHoneyPotThrowExceptionOnError',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformhoneypotcontrol',
            'name'  => 'bTRWFormHoneyPotThrowMessageOnError',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwformhoneypotcontrol',
            'name'  => 'bTRWFormHoneyPotThrowExceptionOnUse',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformhoneypotcontrol',
            'name'  => 'bTRWFormHoneyPotCollectIP',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformhoneypotcontrol',
            'name'  => 'aTRWFormHoneyPotUseMessageForFrontendController',
            'type'  => 'arr',
            'value' => [],
        ],
    ],
];
