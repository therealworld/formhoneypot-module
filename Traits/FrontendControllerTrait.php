<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormHoneyPotModule\Traits;

use ReflectionClass;
use TheRealWorld\FormHoneyPotModule\Core\FormHoneyPotHelper;

/**
 * Trait for FrontendController with Honeypot.
 */
trait FrontendControllerTrait
{
    /** Template variable getter. */
    public function getHoneyPotFormElements(): array
    {
        return FormHoneyPotHelper::getHoneyPotFormElements((new ReflectionClass($this))->getShortName());
    }
}
