<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormHoneyPotModule\Application\Component\Widget;

use ReflectionClass;
use TheRealWorld\FormHoneyPotModule\Core\FormHoneyPotHelper;

/**
 * Article box widget.
 *
 * @mixin \OxidEsales\Eshop\Application\Component\Widget\ArticleDetails
 */
class ArticleDetails extends ArticleDetails_parent
{
    /** Template variable getter. */
    public function getHoneyPotFormElements(): array
    {
        return FormHoneyPotHelper::getHoneyPotFormElements((new ReflectionClass($this))->getShortName());
    }
}
