<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'ERROR_MESSAGE_HONEYPOT_MESSAGE'   => 'Das Formular wurde missbräuchlich genutzt und daher nicht abgesendet.',
    'ERROR_MESSAGE_HONEYPOT_EXCEPTION' => '!Formularmissbrauch! Daten: %s',
    'INFO_MESSAGE_HONEYPOT_EXCEPTION'  => '!Formularnutzung! Daten: %s',
];
