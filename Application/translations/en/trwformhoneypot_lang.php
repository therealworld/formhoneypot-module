<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'ERROR_MESSAGE_HONEYPOT_MESSAGE'   => 'The form was misused and therefore not sent.',
    'ERROR_MESSAGE_HONEYPOT_EXCEPTION' => '!Form abuse! Data: %s',
    'INFO_MESSAGE_HONEYPOT_EXCEPTION'  => '!Form usage! Data: %s',
];
