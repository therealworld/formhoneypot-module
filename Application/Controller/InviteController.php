<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormHoneyPotModule\Application\Controller;

use ReflectionClass;
use TheRealWorld\FormHoneyPotModule\Core\FormHoneyPotHelper;
use TheRealWorld\FormHoneyPotModule\Traits\FrontendControllerTrait;

/**
 * invite class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\InviteController
 */
class InviteController extends InviteController_parent
{
    use FrontendControllerTrait;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function send()
    {
        if (FormHoneyPotHelper::isFallenIntoHoneyPot((new ReflectionClass($this))->getShortName())) {
            return null;
        }

        return parent::send();
    }
}
