<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormHoneyPotModule\Application\Controller;

use ReflectionClass;
use TheRealWorld\FormHoneyPotModule\Core\FormHoneyPotHelper;
use TheRealWorld\FormHoneyPotModule\Traits\FrontendControllerTrait;

/**
 * article details class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\ArticleDetailsController
 */
class ArticleDetailsController extends ArticleDetailsController_parent
{
    use FrontendControllerTrait;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function addMe()
    {
        if (FormHoneyPotHelper::isFallenIntoHoneyPot((new ReflectionClass($this))->getShortName())) {
            $this->_iPriceAlarmStatus = 0;

            return;
        }
        parent::addMe();
    }
}
