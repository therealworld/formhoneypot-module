<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormHoneyPotModule\Application\Controller\Admin;

use stdClass;
use TheRealWorld\FormHoneyPotModule\Core\FormHoneyPotHelper;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;

/**
 * ModuleConfiguration class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration
 */
class ModuleConfiguration extends ModuleConfiguration_parent
{
    /** Template Getter. List of Frontend Controller with Honeypot */
    public function getFrontendControllerWithHoneyPot(): array
    {
        $aResult = [];

        // FrontendController List
        $aSelectedFrontendController = ToolsConfig::getOptionsArray('aTRWFormHoneyPotUseMessageForFrontendController');
        $aFrontendController = FormHoneyPotHelper::getFrontendControllerWithHoneyPot();
        foreach ($aFrontendController as $sFrontendController) {
            $oFrontendController = new stdClass();
            $oFrontendController->name = $sFrontendController;
            $oFrontendController->selected = in_array($oFrontendController->name, $aSelectedFrontendController, true);
            $aResult[] = clone $oFrontendController;
        }

        return $aResult;
    }
}
