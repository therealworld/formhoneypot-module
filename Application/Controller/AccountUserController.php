<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormHoneyPotModule\Application\Controller;

use TheRealWorld\FormHoneyPotModule\Traits\FrontendControllerTrait;

/**
 * user class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\AccountUserController
 */
class AccountUserController extends AccountUserController_parent
{
    use FrontendControllerTrait;
}
