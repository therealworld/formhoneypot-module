<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormHoneyPotModule\Application\Controller;

use ReflectionClass;
use TheRealWorld\FormHoneyPotModule\Core\FormHoneyPotHelper;

/**
 * pricealarm class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\PriceAlarmController
 */
class PriceAlarmController extends PriceAlarmController_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function addme()
    {
        if (FormHoneyPotHelper::isFallenIntoHoneyPot((new ReflectionClass($this))->getShortName())) {
            $this->_iPriceAlarmStatus = 0;

            return null;
        }

        return parent::addme();
    }
}
