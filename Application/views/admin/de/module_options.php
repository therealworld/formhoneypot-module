<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwformhoneypotconfig'  => 'Formular-Optionen',
    'SHOP_MODULE_GROUP_trwformhoneypotcontrol' => 'Kontrolle',

    'SHOP_MODULE_bTRWFormHoneyPotUseHoneyPot'                     => 'Nutze Honigtopf',
    'SHOP_MODULE_aTRWFormHoneyPotFormVars'                        => 'Ergänze die Formularelementnamen die als Honigtopf genutzt werden sollen.',
    'SHOP_MODULE_bTRWFormHoneyPotThrowMessageOnError'             => 'Zeige im Frontend eine Fehlermeldung, wenn der Honigtopf genutzt wurde.',
    'SHOP_MODULE_bTRWFormHoneyPotThrowExceptionOnError'           => 'Erzeuge einen Error-Log-Eintrag wenn jemand in den Honigtopf gefallen ist.',
    'HELP_SHOP_MODULE_bTRWFormHoneyPotThrowExceptionOnError'      => 'Im OXID-Log wird die komplette Formulareingabe gespeichert.',
    'SHOP_MODULE_bTRWFormHoneyPotThrowExceptionOnUse'             => 'Erzeuge einen Info-Log-Eintrag wenn jemand das Formular genutzt hat.',
    'HELP_SHOP_MODULE_bTRWFormHoneyPotThrowExceptionOnUse'        => 'Im OXID-Log wird die komplette Formulareingabe gespeichert. Diese Option sollte nur genutzt werden, wenn scheinbar der Honigtopf umgangen wird. Dann kann man schauen, wie gespamt wird um die versteckten Formularfelder des Honigtopfs noch zu optimieren.',
    'SHOP_MODULE_bTRWFormHoneyPotCollectIP'                       => 'Soll die IP geloggt werden?',
    'HELP_SHOP_MODULE_bTRWFormHoneyPotCollectIP'                  => 'Optional: Bitte besprechen Sie diese Option mit Ihrem Datenschutzbeauftragten. Das loggen der IP im Missbrauchsfall kann korrekt sein, jedoch nicht beim generellen loggen der Formulareingaben.',
    'SHOP_MODULE_aTRWFormHoneyPotUseMessageForFrontendController' => '"Nutzungs"-Logs erzeugen für Formulartyp ... ?',
    'SHOP_MODULE_DescriptionArticleDetailsController'             => 'Preisalarm im Artikel Detail',
    'SHOP_MODULE_DescriptionContactController'                    => 'Kontakt',
    'SHOP_MODULE_DescriptionInviteController'                     => 'Einladung',
    'SHOP_MODULE_DescriptionPriceAlarmController'                 => 'Preisalarm',
    'SHOP_MODULE_DescriptionSuggestController'                    => 'Empfehlung',
    'SHOP_MODULE_DescriptionUserComponent'                        => 'Registrierung',
];
