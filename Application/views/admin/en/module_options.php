<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwformhoneypotconfig'  => 'Form-Options',
    'SHOP_MODULE_GROUP_trwformhoneypotcontrol' => 'Controlling',

    'SHOP_MODULE_bTRWFormHoneyPotUseHoneyPot'                     => 'use honeypot',
    'SHOP_MODULE_aTRWFormHoneyPotFormVars'                        => 'complete the form element names that should be used as a honeypot',
    'SHOP_MODULE_bTRWFormHoneyPotThrowMessageOnError'             => 'Show in the frontend an error message when the honeypot was used.',
    'SHOP_MODULE_bTRWFormHoneyPotThrowExceptionOnError'           => 'Create an error log entry if someone has fallen into the honeypot.',
    'HELP_SHOP_MODULE_bTRWFormHoneyPotThrowExceptionOnError'      => 'The OXID log stores the complete form input.',
    'SHOP_MODULE_bTRWFormHoneyPotThrowExceptionOnUse'             => 'Create an info log entry if someone has used the form.',
    'HELP_SHOP_MODULE_bTRWFormHoneyPotThrowExceptionOnUse'        => 'The OXID log stores the complete form input. This option should only be used if the honey pot seems to be bypassed. Then you can see how it is being made to optimize the hidden honeypot form fields.',
    'SHOP_MODULE_bTRWFormHoneyPotCollectIP'                       => 'Should the IP be logged?',
    'HELP_SHOP_MODULE_bTRWFormHoneyPotCollectIP'                  => 'Optional: Please discuss this option with your privacy officer. The logging of the IP in case of abuse can be correct, but not with the general logging of the form input.',
    'SHOP_MODULE_aTRWFormHoneyPotUseMessageForFrontendController' => 'Generate "Use"-logs for formtype ... ?',
    'SHOP_MODULE_DescriptionArticleDetailsController'             => 'Pricealarm in Article Detail',
    'SHOP_MODULE_DescriptionContactController'                    => 'Contact',
    'SHOP_MODULE_DescriptionInviteController'                     => 'Invitation',
    'SHOP_MODULE_DescriptionPriceAlarmController'                 => 'Pricealarm',
    'SHOP_MODULE_DescriptionSuggestController'                    => 'Suggest',
    'SHOP_MODULE_DescriptionUserComponent'                        => 'Register',
];
