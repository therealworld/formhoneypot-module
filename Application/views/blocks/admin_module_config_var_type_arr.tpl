[{if $oView|method_exists:'getFrontendControllerWithHoneyPot' && $module_var=='aTRWFormHoneyPotUseMessageForFrontendController'}]
    [{assign var="aForms" value=$oView->getFrontendControllerWithHoneyPot()}]
    <input id="input_confarrs_[{$module_var}]" type="hidden" name="confarrs[[{$module_var}]][]" value="" />
    <select multiple style="width: 210px;" onChange="transferSelected[{$module_var}](this)">
        [{foreach from=$aForms key=sKey item=oForm}]
            <option value="[{$oForm->name}]" [{if $oForm->selected}]selected[{/if}]>[{'SHOP_MODULE_Description'|cat:$oForm->name|oxmultilangassign}]</option>
        [{/foreach}]
    </select>
    [{capture assign="sTransferJS"}]
        [{strip}]
            function transferSelected[{$module_var}](sel) {
                var opts = [],
                    opt,
                    len = sel.options.length;
                for (var i = 0; i < len; i++) {
                    opt = sel.options[i];
                    if (opt.selected) {
                        opts.push(opt.value);
                    }
                }
                var elem = document.getElementById('input_confarrs_[{$module_var}]');
                elem.value = opts.join(" \n ");
                console.log(elem.value);
            }
        [{/strip}]
    [{/capture}]
    [{oxscript add=$sTransferJS priority=10}]
[{else}]
    [{$smarty.block.parent}]
[{/if}]