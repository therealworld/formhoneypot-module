OXID6 MailRepeat Module
======

![vendor-logo the-real-world.de](picture.png)

### Features

* Add a hidden honeypot to sensitive forms
* Forms: contact, login, register, account, invite, pricealarm, articlesuggest

### Themeing

* 100% Compatible for WAVE or FLOW Standard-Theme

### Options in Module-Backend

* activate the Module
* Optional: the hidden individual varibales
* Optional: throw message on error in frontend
* Optional: throw message on "fallen in honeypot" in errorlog
* Optional: throw message on "use form" in errorlog
* Optional: collect the IP

### Module installation via composer

In order to install the module via composer run one of the following commands in commandline in your shop base directory
(where the shop's composer.json file resides).
* **composer require therealworld/formhoneypot-module** to install the actual version compatible with OXID6

## Bugs and Issues

If you experience any bugs or issues, please report them in on https://bitbucket.org/therealworld/formhoneypot-module/issues.

